import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Persona} from '../interfaces/personas';


@Injectable({
  providedIn: 'root'
})
export class PersonasService {
  ENDPOINT ='http://127.0.0.1:8000/api';
  constructor(private HttpClient:HttpClient) { }
  get(){
    return this.HttpClient.get(this.ENDPOINT + '/personas');
  }
  post(persona:Persona){
    console.log('hola desde el crear',persona)
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.HttpClient.post(this.ENDPOINT + '/personas',persona,{headers:headers})
    this.get()
  }
  put(persona){
    console.log('hola desde actualizar',persona)
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.HttpClient.put(this.ENDPOINT + '/personas/' + persona.id,persona,{headers:headers})
  }
  delete(id){
    console.log('hola desde eliminar',id)
    return this.HttpClient.delete(this.ENDPOINT + '/personas/' + id);
    this.get()
  }
}

