export interface Persona{
    id?:number;
    nombre:string;
    apellido:string;
    telofono:number;
    correo:string;
    direccion:string;
    create_at:string;
    updated_at:string;
}