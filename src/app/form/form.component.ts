import { Component, OnInit } from '@angular/core';
import {PersonasService} from '../Services/personas.service';
import {Persona} from '../interfaces/personas';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
      
      persona: Persona={
      nombre:null,
      apellido:null,
      correo:null,
      telofono:null,
      direccion:null,
     
    };
  id:any
  editar: boolean = false;
  personas1:Persona[];
  constructor(private PersonasService:PersonasService,private ActivatedRoute:ActivatedRoute) { 
    this.id = this.ActivatedRoute.snapshot.params['id'];
    if(this.id){
      this.editar = true
      this.PersonasService.get().subscribe((data:Persona[])=>{
      console.log('llego la consulta',data);
      this.personas1 = data;
      this.persona = this.personas1.find(el => el.id == this.id)
      console.log(this.persona)
      })
    }
  }

  ngOnInit(): void {
  
  }
   post(){
     try {
       if(this.editar){
         this.editarResgistro()
       }else{
         this.PersonasService.post(this.persona).subscribe((data)=>{
          console.log('guardando',data)
          alert('guardado correctamente');
         
         })
       }
     } catch (e) {
       console.log(e)
     }
}
editarResgistro(){
try {
  this.PersonasService.put(this.persona).subscribe((data)=>{
    console.log('Actualizando',data)
    alert('guardado correctamente');
 })
} catch (e) {
  console.log(e)
}

}
}
