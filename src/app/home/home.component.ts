import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {PersonasService} from '../Services/personas.service';
import {Persona} from '../interfaces/personas';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  personas:Persona[]
  constructor(private PersonaService:PersonasService, private httpcliente:HttpClient) {
    this.PersonaService.get().subscribe((data:Persona[])=>{
      this.personas = data
    })
   }

  ngOnInit(): void {
  }
  delete(id){
    try {
      this.PersonaService.delete(id).subscribe((data)=>{
        alert('se elemino el registro');
      })
    } catch (e) {
      console.log(e)
    }
  }
}
